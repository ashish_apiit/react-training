import React from 'react';
import { Switch, Route } from 'react-router';
import Dashboard from './components/Dashboard';

class PrivateRoutes extends React.Component{

    render(){
        return(
            <React.Fragment>
                <h1>Hi Private Routes Loaded..</h1>
                <Switch>
                    <Route match={this.props.match} path={`${this.props.match.path}/dashboard`} component={Dashboard}></Route>
                </Switch>
            </React.Fragment>
        )
    }
        
}
export default PrivateRoutes;