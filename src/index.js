import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {AWS_KEY, AWS_SECRET_KEY} from './config';
import {BrowserRouter as Router} from 'react-router-dom';
// ES 6 - Import feature 
// Javascript ES 6
// 'use strict'
// name = "Ashish"
// Download a HTML Template Oneline free template
// {name, email} = {"name": "Ashish", "email": "ksadfuasdf"}
// var some = a.name;
// 
console.log("config", AWS_SECRET_KEY);

ReactDOM.render(
  <React.StrictMode>
    <Router>
      <App />
    </Router>
   
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
