import React from 'react';
import Comment from './Comment';

function parentComment(){
    return (
        <h1>Post</h1>
    )
}

function Post(){
    function updateText(data){
        console.log("update text", data);
    }
    return (
        <div>
            { parentComment()}
            <Comment updateText = { updateText(data) } image = "/img/download.png" content="content for comment" />
            <div className="childComment" style={{ marginLeft: 130 }}>
                <Comment image = "/img/download.png" content="content for comment2" ></Comment>
                <Comment image = "/img/download.png" content="content for comment3"  />
                <Comment  image = "/img/download.png" content="content for comment4"  />
            </div>
            <Comment image = "/img/download.png" content="content for comment2" ></Comment>
            <div className="childComment" style={{ marginLeft: 130 }}>
                <Comment image = "/img/download.png" content="content for comment2" ></Comment>
                <Comment image = "/img/download.png" content="content for comment3"  />
                <Comment  image = "/img/download.png" content="content for comment4"  />
            </div>
            <Comment image = "/img/download.png" content="content for comment3"  />
            <Comment  image = "/img/download.png" content="content for comment4"  />
        </div>
    
    )
}

export default Post;