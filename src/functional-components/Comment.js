import React from 'react';


function Comment(props){

    return (
        <React.Fragment>
            <div className="row">
                <div className="col-3"><img alt="" src={props.image} /></div>
                <div className="col-9">{props.content}</div>
            </div>
            <div className="row">
                <div className="col-3"><img alt="" src={props.image} /></div>
                <div className="col-9">{props.content}</div>
            </div>
        </React.Fragment>

    )
}

export default Comment;