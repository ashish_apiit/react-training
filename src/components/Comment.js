import React from 'react';
import PropTypes from 'prop-types'

class Comment extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            color: 'red',
            background: 'blue'
        }
    
    }

    myObj = {
        "name":"Ashish",
        "email": "kumar.ashish034@gmail.com",
        mobile: '8088615148'
    }
    componentWillMount(){
        this.setState({color: "white"});
        console.log(this.state.color);
        //alert("Component will mount called");
    }
    componentDidMount(){
        //alert("Component will did mount called");
    }

    componentWillUpdate(){
        let a = this.props;
        //let a = Object.assign({}, this.props);
    }
    componentDidUpdate(prevProp, nextProp){
        if(prevProp.state !== nextProp.state){
            this.setState({color: "Black"});
        }
    }
    componentWillUnmount(){
        
    }
    render(){
      
       // alert("render function called")
        return (
                <div className="row">
                    <div className="col-3" onClick={() => this.props.callPost(this.myObj)}><img alt="" src={this.props.image} /></div>
                    <div className="col-9">{this.props.content}</div>
                </div>
        )
    }
    
}


Comment.defaultProps = {
    image: '/img/download.png',
    content: 'Not Available'
}

Comment.propTypes ={
    image: PropTypes.string.isRequired,
    content: PropTypes.string

}

export default Comment;