import React from 'react';
import Comment from './Comment';
import {withRouter} from 'react-router';
const DisplayParam = (props) =>{
    return(
        <div>{props.id}</div>
    )
}

class Post extends React.Component{
    constructor(){
        super();
        this.state = {
            title: "Post from State"
        }
        //this.changeTitle = this.changeTitle.bind(this);
    }
    myObj = {
        callPost: (obj) => this.propsFunction(obj),
         image : "/img/download.png",
        content: "content for comment"
    }
    parentComment(){
        
        return (
            <h1>{this.state.title}</h1>
        )
    }
    componentDidMount(){
       // console.log(this.props.match.params.id);
    }
    changeTitle(){
        this.setState({ title: "Updated Title"});
    }
    propsFunction(myobj){
        console.log("This is from parent component", myobj);
    }
    render(){
        return (
            <div>
              
                { this.parentComment()}
                <button onClick={() => this.changeTitle()}>Click Me</button>
                <Comment  />
               
            </div>
        
        )
    }
}

export default withRouter(Post);