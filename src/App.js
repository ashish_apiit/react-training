import React from 'react';
import Post from './components/post';
import {BrowserRouter as Router, Switch, Route, Link} from 'react-router-dom';
import NotFound from './not-found'
import PrivateRoutes from './private-routes';
import Private from './private';


function App(props) {
  var data = [
    {image: "", content: 'comment data'}, 
    {image: "", content: 'comment data'}, 
    {image: "", content: 'comment data'}, 
  ]
  return (
    <div className="App">
      <div className="container-fluid">
        <Header></Header>
        
        
          <Link to="/blog">Blog</Link>
          <Switch>
            <Route exact path="/blog" component={Post}></Route>
            <Private path="/admin" component = {PrivateRoutes} history={props.history}>

            </Private>
            
            <Route exact path="/" component={Home}></Route>
            <Route component={NotFound}></Route>
          </Switch>
        
        <Footer></Footer>
      </div>
    </div>
  );
}
const Header =() =>{
  return (
    <>
    <div>My First Website Header</div>
    </>
    )
  
}
const Footer = () =>{
  return (
    <>
    <div>My First Website Footer</div>
  </>
  )
  
}
const Home = () =>{
  return (
    <>
    <div>My Home Page</div>
  </>
  )
  
}
export default App;
