import React from 'react';
import { Switch, Route, Redirect } from 'react-router';

const Private = ({component: Component, path, ...rest }) => {

    const render = props => {
        let isAuthenticated = false;
       return isAuthenticated === true ? <Component {...props} /> : <Redirect to='/' />;
    }
    return <Route path={path} render={render} {...rest} />
    
}
// React app, Home, about-us, service, admin - Navigation - Git URL =>
export default Private;